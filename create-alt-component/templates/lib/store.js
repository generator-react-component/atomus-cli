import actions from "./actions"
import dependencies from "../dependencies"

var {Alt} = dependencies

export default Alt.createStore(class <%= classifiedComponentName %> {
  static config = {
    stateKey: "state"
  };
  state = {
    // you might want to change this
    data: null,
    errors: {}
  };
  constructor() {
    this.bindActions(actions)
  }
  // you might want to change 'data'
  onSet(data) {
    this.state.data = data
    this.state.errors = {}
  }
  onSetError(errors) {
    if (errors) this.state.errors = errors
  }
}, "<%= classifiedComponentName %>")
