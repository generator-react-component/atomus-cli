import dependencies from "../dependencies"

var {ApiRequest, Alt} = dependencies

export default Alt.createActions(class <%= classifiedComponentName %> {
  static displayName = "<%= classifiedComponentName %>";
  constructor() {
    this.generateActions(
      "set",
      "setError"
    )
  }
  // you change this
  async create(data) {
    try {
      // you change this
      let result = await ApiRequest.post("/data").send(data)
      this.actions.set(result.body)
    } catch (error) {
      this.actions.setError(error.body)
    }
  }
})
