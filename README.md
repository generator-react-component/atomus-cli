# React Components Generator #

### Description ###

* ReactJS framework alongside with stylus

### Prerequisites? ###

* NODEJS

### Installation ###

```
$ npm install -g generator-atomus
```

### Usage ###
```
yo atomus
yo atomus:install-component
yo atomus:create-component
yo atomus:create-alt-component
yo atomus:install-component-deps
yo atomus:check-changes
yo atomus:check-update

```
### RUN SERVER ###
```
gulp clean,run
```

### RUN IN MOBILE ###
```
gulp clean,build,run --mobile
```

### Contribution guidelines ###

* Writing tests
* Code review
