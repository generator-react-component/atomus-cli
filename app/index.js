'use strict';
var util = require('util'),
  path = require('path'),
  yeoman = require('yeoman-generator'),
  chalk = require('chalk'),
  shell = require('shelljs');

var AtomusGenerator = yeoman.generators.Base.extend({
  init: function(){
    // invoke npm install on finish
    this.on('end', function() {
      // console.log("END?")
      if (!this.options['skip-install']) {
        // console.log("INSTALL?")
        // shell.exec('NODE_ENV=development; npm install && jspm install && bower install');
      }
    });
    // have Yeoman greet the user
    console.log(this.yeoman);

    // replace it with a short and sweet description of your generator
    console.log(chalk.magenta('You\'re using the Atomic generator.'));
  },
  askForApplicationDetails: function(){
    var done = this.async();

    var prompts = [{
      name: 'appName',
      message: 'What would you like to call your application?',
      default: 'atomicProject'
    },{
      name: 'appDescription',
      message: 'What would be the description of the application?',
      default: 'Atomic Project'
    },{
      name: 'appRepository',
      message: 'What is the repository of this project?',
      default: ''
    }, {
      name: 'appAuthor',
      message: 'What is the name of the author?',
      default: ''
    }];

    this.prompt(prompts, function(props) {
      this.appName = props.appName;
      this.appDescription = props.appDescription;
      this.appRepository = props.appRepository;
      this.appAuthor = props.appAuthor;

      this.slugifiedAppName = this._.slugify(this.appName);
      this.humanizedAppName = this._.humanize(this.appName);

      done();
    }.bind(this));
  },
  copyApplicationFolder: function(){
    this.mkdir('src');
    this.directory('src');
  },

  renderApplicationDependenciesFiles: function() {
    this.template('bower.json', 'bower.json');
    this.template('config.js', 'config.js');
    this.template('favicon.ico', 'favicon.ico');
    this.template('Gulpfile.babel.js', 'Gulpfile.babel.js');
    this.template('index.nunjucks', 'index.nunjucks');
    this.template('package.json', 'package.json');
    this.template('README.md', 'README.md');

    this.template('_babelrc', '.babelrc');
    this.template('_editorconfig', '.editorconfig');
    this.template('_eslintrc', '.eslintrc');
    this.template('_gitignore', '.gitignore');
    this.template('_stylintrc', '.stylintrc');
    this.template('atomic.json', 'atomic.json');

    this.copy('src/main.styl');
    this.copy('src/components.styl');
    this.copy('src/variables.styl');
    this.copy('src/variables.js');
  },
});

module.exports = AtomusGenerator;
