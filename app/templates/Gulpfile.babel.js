import fs from "fs"
import gulp from "gulp"
import gulpPlugins from "gulp-load-plugins"
import runSequence from "run-sequence"
import del from "del"
import gutil from "gulp-util"
import minimist from "minimist"

let $ = gulpPlugins()
let argv = minimist(process.argv.slice(1))
let MOBILE = !!argv.mobile
let CORDOVA = argv.cordova == null ? true : !!argv.cordova

// Lint
// -----------------------------------------------------------------------------
gulp.task("lint", function(cb) {
  runSequence("lint:js", "lint:stylus", cb)
})

// Build (Web/Mobile)
// -----------------------------------------------------------------------------
gulp.task("build", function(cb) {
  runSequence(
    "clean",
    "symlink:bower_components",
    "nunjucks",
    ["static", "stylus", "jspm", "html", "fonts", "images"],
    ["minify:js", "minify:html", "minify:polyfill", "minify:css"],
    "revision",
    "revision:replace",
    // "compact", // if uncomment this. bower_components will be deleted before apk is being compiled
  cb)
})

// Test (Web/Mobile)
// -----------------------------------------------------------------------------
gulp.task("test", function(cb) {
  runSequence("webdriver_update", "protractor", cb);
});

// Run (Web/Mobile)
// -----------------------------------------------------------------------------
gulp.task("run", () => {
  if (MOBILE && CORDOVA) {
    return gulp.src("").pipe($.shell([
      "cordova prepare",
      "cordova run android"
    ]))
  } else {
    let build = fs.existsSync("www/index.html")
    if (build) {
      return runSequence("serve", "watch")
    } else {
      return runSequence(["nunjucks", "stylus"], "serve", "watch")
    }
  }
})


// Post-build cleanup
gulp.task("compact", function(cb) {
  del(["www/bower_components"], cb)
})

// Compile: Stylus (.styl) >> CSS
gulp.task("stylus", () => {
  return gulp.src(["./src/main.styl"])
    .pipe($.sourcemaps.init())
    .pipe($.stylus({use: [require("kouto-swiss")()]}))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest("./www/css"))
    .pipe($.connect.reload())
})

// Compile: HTML
gulp.task("html", () => {
  return gulp.src(["./index.html"])
    .pipe($.htmlReplace({
      "js": "js/main.js",
      "css": "css/main.css",
      "polyfill": "js/polyfill.js"
    }))
    .pipe(gulp.dest("./www"))
})

// Compile: Nunjucks
gulp.task("nunjucks", () => {
  $.nunjucksRender.nunjucks.configure(".", {
    watch: false
  })

  return gulp.src(["./index.nunjucks"])
    .pipe($.nunjucksRender({
      mobile: MOBILE,
      cordova: CORDOVA,
    }))
    .pipe(gulp.dest("."))
})

// Compile: Javscript (bundle through jspm)
gulp.task("jspm", $.shell.task([
  "jspm bundle-sfx src/Browser/index www/js/main.js --skip-source-maps"
]))

// Symlink: bower_components
gulp.task("symlink:bower_components", () => {
  return gulp.src("bower_components")
    .pipe($.symlink("./www/bower_components", {force: true}))
})

// Minify: JS
gulp.task("minify:js", () => {
  return gulp.src("./www/js/*.js")
    .pipe($.uglify())
    .pipe(gulp.dest("www/js"))
})

// Minify: Polyfill
gulp.task("minify:polyfill", () => {
  return gulp.src([
      "./bower_components/es5-shim/es5-shim.js",
      "./bower_components/es5-shim/es5-sham.js",
      "./bower_components/html5shiv/dist/html5shiv.js",
    ])
    .pipe($.concat("polyfill.js"))
    .pipe($.uglify())
    .pipe(gulp.dest("www/js"))
})

// Minify: CSS
gulp.task("minify:css", () => {
  return gulp.src("./www/css/*.css")
    .pipe($.minifyCss({
      keepBreaks: false,
      rebase: false,
    }))
    .pipe(gulp.dest("./www/css/"))
})

// Minify: HTML
gulp.task("minify:html", () => {
  return gulp.src("./www/*.html")
    .pipe($.minifyHtml({}))
    .pipe(gulp.dest("./www/"))
})

// CSS and JavaScript Asset Revisioning
gulp.task("revision", () => {
  return gulp.src(["!www/bower_components/**/*", "www/**/*.css", "www/**/*.js"])
    .pipe($.rev())
    .pipe(gulp.dest("www"))
    .pipe($.rev.manifest())
    .pipe(gulp.dest("www"))
})

gulp.task("revision:replace", () => {
  let manifest = gulp.src("./www/rev-manifest.json")

  return gulp.src("www/index.html")
    .pipe($.revReplace({manifest: manifest}))
    .pipe(gulp.dest("www"))
    .on("end", () => {
      return del([
        "www/rev-manifest.json",
        "www/css/main.css",
        "www/js/main.js",
        "www/js/polyfill.js",
      ])
    })
})

// Fonts
// TODO: Figure out an automatic way of doing this
gulp.task("fonts", () => {
  return gulp.src([
    "bower_components/font-awesome/fonts/*",
    "bower_components/roboto-fontface/fonts/*",
    "bower_components/material-design-iconic-font/dist/fonts/*"
  ]).pipe(gulp.dest("www/fonts/"))
})

// Images
// TODO: Figure out an automatic way of doing this
gulp.task("images", () => {
  return gulp.src([
    "images/**/*"
  ]).pipe(gulp.dest("www/images/"))
})

// Watch
gulp.task("watch", () => {
  gulp.watch(["./src/**/*.styl"], ["stylus"])

  gulp.watch(["./index.html"], () => {
    return gulp.src("./index.html").pipe($.connect.reload())
  })

  gulp.watch(["./src/**/*.js*"], () => {
    return gulp.src("./src/**/*.js*").pipe($.connect.reload())
  })
})

// Clean
gulp.task("clean", function(cb) {
  del(["www"], cb)
})

// Lint: JavaScript
gulp.task("lint:js", () => {
  return gulp.src(["src/**/*.js"])
    .pipe($.eslint())
    .pipe($.eslint.format())
    .pipe($.eslint.failOnError())
})

// Lint: Stylus
gulp.task("lint:stylus", () => {
  return gulp.src(["src/**/*.styl"])
    .pipe($.stylint({config: ".stylintrc"}))
})

// Download local selenimum webdrivers
gulp.task("webdriver_update", $.protractor.webdriver_update);

// Execute and run the protractor test suite
gulp.task("protractor", function(cb) {
  let base = $.protractor.getProtractorDir();
  $.connect.server({root: "www", fallback: "index.html"});

  setTimeout(() => {
    gulp.src(["./test/spec/**/*.js"])
      .pipe($.protractor.protractor({
          configFile: "test/protractor.config.js",
          args: [
            "--baseUrl", "http://127.0.0.1:8080",
          ]
      }))
      .on("error", function(e) { throw e })
      .on("end", () => {
        $.connect.serverClose();
        cb();
      });
  }, 250);
});

// Serve (Web)
gulp.task("serve", () => {
  let build = fs.existsSync("www/index.html")
  return $.connect.server({
    root: build ? "www" : ".",
    livereload: !build,
    middleware: () => {
      return [
        require("connect-history-api-fallback")(),
      ]
    }
  })
})

// Allow for adhoc task sequences to be created from the command line
function createRunSequenceTask(taskName, taskSets) {
  return gulp.task(taskName, function(done) {
    runSequence.apply(null,
      Array.prototype.slice.call(taskSets).concat([done]));
  })
}

gutil.env._.forEach( function(task) {
  if (/(,|\[)/.test(task)) {
    let json = task.replace(/([^,\[\]]+)/g, '"$1"');
    let obj = JSON.parse('{"taskSets": [' + json + ']}');
    createRunSequenceTask(task, obj.taskSets);
  }
})
