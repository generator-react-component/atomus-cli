# React Components Generator #

### Description ###

* ReactJS framework alongside with stylus

### Prerequisites? ###

* NODEJS

### Usage ###
```
yo rac
yo rac install-component
yo rac create-component
yo rac install-component-deps
yo rac check-changes

```
### RUN SERVER ###
```
gulp clean,run
```

### RUN IN MOBILE ###
```
gulp clean,build,run --mobile
```

### Contribution guidelines ###

* Writing tests
* Code review
