import React, { PropTypes } from "react"

import dependencies from "./dependencies"

const <%= classifiedComponentName %> = React.createClass({
  render() {
    return (
      <div className="<%= slugifiedComponentName %> ">
        <%= humanizedComponentName %>
      </div>
    )
  }
})

export default <%= classifiedComponentName %>
