"use strict";
var util = require("util"),
  path = require("path"),
  yeoman = require("yeoman-generator"),
  chalk = require("chalk"),
  _ = require("lodash"),
  fs = require("fs"),
  IsThere = require("is-there"),
  exec = require('child_process').exec;

var AtomusGenerator = yeoman.generators.Base.extend({
  init: function(){
    // invoke npm install on finish
    this.on("end", function() {
      // if (!this.options["skip-install"]) {
      //   this.npmInstall();
      // }
    });
    // have Yeoman greet the user
    console.log(this.yeoman);

    // replace it with a short and sweet description of your generator
    console.log(chalk.magenta("You\"re using the Atomic generator."));
  },
  check: function() {
    var done = this.async();
    var t = this;
    var currentComponentIndex = 0;
    var askForCommentThenPush = function(components, component) {
      var prompts1 = [{
        type: "confirm",
        name: "continueCommit",
        message: "You want to commit changes?",
        default: "n"
      }];

      t.prompt(prompts1, function(props1) {
        if(props1.continueCommit) {
          var prompts = [{
            name: "commitComment",
            message: "You are commiting changes from: " + component + " Component. Please add comment:",
            default: "Fixing some bugs..."
          }];

          t.prompt(prompts, function(props) {
            exec("cd " + path.resolve("./src/" + component) + " && git add --all", function(error, stdout, stderr) {
              exec("cd " + path.resolve("./src/" + component) + " && git commit -m \"" + props.commitComment + "\"", function(error, stdout, stderr) {
                exec("cd " + path.resolve("./src/" + component) + " && git push origin master", function(error, stdout, stderr) {
                  currentComponentIndex += 1;
                  commitChanges(components, currentComponentIndex);
                });
              });
            });
          });
        } else {
          currentComponentIndex += 1;
          commitChanges(components, currentComponentIndex);
        }
      });
    };
    var commitChanges = function(components, indexComponent) {
      var component = components[indexComponent];
      if (component !== undefined) {
        if (IsThere(path.resolve("./src/" + component + "/.git"))) {
          console.log("Checking " + component);

          exec("cd " + path.resolve("./src/" + component) + " && git diff", function(error, stdout, stderr) {
            console.log(stdout)
            exec("cd " + path.resolve("./src/" + component) + " && git status", function(error2, stdout2, stderr2) {
              if (stdout2.split("nothing to commit").length!==2) {
                askForCommentThenPush(components, component);
              } else {
                currentComponentIndex += 1;
                commitChanges(components, currentComponentIndex);
              }
            });
          });
        } else {
          currentComponentIndex += 1;
          commitChanges(components, currentComponentIndex);
        }
      } else {
        done();
      }
    };
    fs.readdir(path.resolve("./src"), function(err, components) {
      commitChanges(components, currentComponentIndex);
    });
  }
});

module.exports = AtomusGenerator;
