var _ = require("lodash"),
  shell = require("shelljs"),
  IsThere = require("is-there"),
  path = require("path");

var generateStyles = function(atomic, isComponentDep) {
  var styles = "";
  _.each(this.atomic.AtomicDeps, function(deps) {
    // console.log(path.resolve("./" + (isComponentDep ? "../" : "src/") + deps.Name + "/style.styl"))
    if (IsThere(path.resolve("./" + (isComponentDep ? "../" : "src/") + deps.Name + "/style.styl"))) {
      var pathname = "./" + deps.Name + "/style";
      styles += "@require \"" + pathname + "\"\n";
    }
  });
  this.componentsStyles = styles;

  shell.exec("rm -rf " + path.resolve("./" + (isComponentDep ? "../" : "src/") + "components.styl"));
  this.template("components.styl", path.resolve("./" + (isComponentDep ? "../" : "src/") + "components.styl"));
};

module.exports = generateStyles;
