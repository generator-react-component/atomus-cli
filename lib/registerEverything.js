var path = require("path"),
    _ = require("lodash"),
    shell = require("shelljs");

var registerEverything = function(settings) {
  var componentFound = false;
  _.each(this.atomic.AtomicDeps, function(existingDeps) {
    if (existingDeps.Name === settings.Name) {
      componentFound = true;
    }
  });
  if (!componentFound) {
    console.log(settings)
    console.log("Registering " + settings.Name);
    this.atomic.AtomicDeps.push(settings);
  }
  this.atomic.cssVariables = this.atomic.cssVariables ? this.atomic.cssVariables : {};
  _.each(settings.cssVariables, function(value, key) {
    // make sure we dont overwrite the existing variables
    if (!this.atomic.cssVariables[key]) {
      this.atomic.cssVariables[key] = value;
    }
  }.bind(this));
  this.atomic.jsVariables = this.atomic.jsVariables ? this.atomic.jsVariables : {};
  _.each(settings.jsVariables, function(value, key) {
    // make sure we dont overwrite the existing variables
    if (!this.atomic.jsVariables[key]) {
      this.atomic.jsVariables[key] = value;
    }
  }.bind(this));
  this.atomic.JSPMDeps = this.atomic.JSPMDeps ? this.atomic.JSPMDeps : {};
  _.each(settings.JSPMDeps, function(installed, jspmPackage) {
    if (!this.atomic.JSPMDeps[jspmPackage]) {
      // set as false.. meaning not installed
      this.atomic.JSPMDeps[jspmPackage] = installed;
    }
  }.bind(this));
};

module.exports = registerEverything
