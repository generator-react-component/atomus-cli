var _ = require("lodash"),
    shell = require("shelljs"),
    path = require("path"),
    glob = require("glob"),
    fs = require("fs-extra"),
    _ = require("lodash");

var copyComponentImages = function(atomic, isComponentDep, isLocalComponentOnly) {
  glob(path.resolve("./src/**/**/images/*"), function (er, files) {
    // files is an array of filenames.
    // If the `nonull` option is set, and nothing
    // was found, then files is ["**/*.js"]
    // er is an error object or null.
    _.each(files, function(file) {
      fs.copy(file, path.resolve("./images/" + path.basename(file)), function (err) {
        if (err) return console.error(err)
        console.log(file + " success copying!")
      }) // copies file
    });
  });
};

module.exports = copyComponentImages;
