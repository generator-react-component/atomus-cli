var _ = require("lodash"),
  shell = require("shelljs");

var jspmInstall = function(atomic) {
  _.each(atomic.JSPMDeps, function(installed, jspm) {
    if (!installed) {
      shell.exec("jspm install " + jspm + "@" + installed);
      // set as true (meaning installed)
      atomic.JSPMDeps[jspm] = true;
    }
  });
};

module.exports = jspmInstall;
