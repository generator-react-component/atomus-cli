var _ = require("lodash"),
  shell = require("shelljs"),
  path = require("path"),
  json = require('format-json');

var generateVariables = function(atomic, isComponentDep) {
  var variables = "";
  var jsVariables = {};
  _.each(this.atomic.cssVariables, function(value, key) {
    variables += key + " = " + value + "\n";
  });
  this.variables = variables;
  this.jsVariables = json.plain(this.atomic.jsVariables);
  shell.exec("rm -rf " + path.resolve("./" + (isComponentDep ? "../" : "src/") + "variables.styl"));
  shell.exec("rm -rf " + path.resolve("./" + (isComponentDep ? "../" : "src/") + "variables.js"));
  this.template("variables.styl", path.resolve("./" + (isComponentDep ? "../" : "src/") + "variables.styl"));
  this.template("variables.js", path.resolve("./" + (isComponentDep ? "../" : "src/") + "variables.js"));
};

module.exports = generateVariables;
